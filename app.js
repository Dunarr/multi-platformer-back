const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server,{serveClient: false, cors: {origin: "*", methods: ["GET", "POST"]}});


io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on("update-player", e => {
        e.player = socket.id
        socket.broadcast.emit("player-updated",e)
    })
});


server.listen(3000, () => {
    console.log('listening on *:3000');
});
# VIA - Multiplayer platformer - Back
The Server of the game, ~~may~~ must be optimized

Require node >= 16

## Run the project
install dependencies:
```shell
npm install
```

start the server (using node or nodemon)
```shell
node app.js
```